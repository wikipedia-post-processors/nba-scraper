﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WikipediaScrapeNBA
{
    public class PlayerStats
    {
        [JsonPropertyName("team")]
        public string Team { get; set; }
        [JsonPropertyName("year")]
        public int Year { get; set; }
        [JsonPropertyName("GP")]
        public int GamesPlayed { get; set; }
        [JsonPropertyName("GS")]
        public int GamesStarted { get; set; }
        [JsonPropertyName("MPG")]
        public float MinutesPerGame { get; set; }
        [JsonPropertyName("FG%")]
        public float FieldGoalPercentage { get; set; }
        [JsonPropertyName("3P%")]
        public float ThreePointPercentage { get; set; }
        [JsonPropertyName("FT%")]
        public float FreeThrowPercentage { get; set; }
        [JsonPropertyName("RPG")]
        public float ReboundsPerGame { get; set; }
        [JsonPropertyName("APG")]
        public float AssistsPerGame { get; set; }
        [JsonPropertyName("SPG")]
        public float StealsPerGame { get; set; }
        [JsonPropertyName("BGP")]
        public float BlocksPerGame { get; set; }
        [JsonPropertyName("PPG")]
        public float PointsPerGame { get; set; }
    }
}
