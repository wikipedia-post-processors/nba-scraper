﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WikipediaScrapeNBA.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        // GET: api/<PlayerController>
        [HttpGet]
        public IEnumerable<int> Get(string firstname, string lastname)
        {
            return new int[] { 2003, 2004, 2005, 
                2006, 2007, 2008, 2009, 2010, 2011, 2012, 
                2013, 2014, 2015, 2016, 2017, 2018, 2019, 
                2020, 2021 };
        }

        // GET api/<PlayerController>/5
        [HttpGet("{year}")]
        public Player Get(string firstname, string lastname, int year)
        {
            Player output = new Player
            {
                awards = new List<string> { "MVP", "LVP" },
                teams = new List<string> { "Cavs", firstname, lastname },
                yearData = new List<PlayerStats>
                {
                    new PlayerStats 
                    {
                        Team = "LeBron's Team",
                        PointsPerGame = 20.0f,
                        Year = year,
                        GamesPlayed = 80,
                        GamesStarted = 79,
                        MinutesPerGame = 35.4f,
                        FieldGoalPercentage = 47.2f,
                        ThreePointPercentage = 33.3f,
                        FreeThrowPercentage = 84.8f,
                        ReboundsPerGame = 6.8f,
                        AssistsPerGame = 3.9f,
                        StealsPerGame = 2.1f,
                        BlocksPerGame = 0.7f,
                    }
                }
            };
            return output;
        }
    }
}
