﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WikipediaScrapeNBA
{
    public class Player
    {
        public List<string> teams { get; set; }
        public List<string> awards { get; set; }
        public List<PlayerStats> yearData { get; set; }
    }
}
